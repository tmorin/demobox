import * as sinon from 'sinon';
import * as chaiAsPromised from 'chai-as-promised';
import {expect, use} from 'chai';
import * as moment from 'moment';
import {Voting, VotingMode} from '../src/voting';
import {OwnResult} from '../src/commands';
import {DummyBallot, DummyHandler, DummyRepository} from './dummy';

use(chaiAsPromised);

describe('commands', () => {
    let repository: DummyRepository;
    let handler: DummyHandler;
    const owners = ['signer0', 'signer1'];
    const candidates = ['candidate0', 'candidate1'];

    beforeEach(function () {
        repository = new DummyRepository();
        handler = new DummyHandler(repository);
    });

    context('when handling RegisterVotingCmd', function () {
        it('should succeed', async function () {
            sinon.stub(repository, 'persistVoting').returns(Promise.resolve());
            await expect(handler.registerVoting({
                signer: owners[0],
                votingId: '#1',
                name: '#1',
                mode: VotingMode.condorcet,
                owners,
                candidates,
                countingOpenedFrom: moment().add(1, 'h').toISOString(),
                countingOpenedTo: moment().add(2, 'h').toISOString()
            })).to.be.fulfilled;
        });
        it('should failed when persistVoting failed', async function () {
            sinon.stub(repository, 'persistVoting').returns(Promise.reject(new Error()));
            await expect(handler.registerVoting({
                signer: owners[0],
                votingId: '#1',
                name: '#1',
                mode: VotingMode.condorcet,
                owners,
                candidates,
                countingOpenedFrom: moment().add(1, 'h').toISOString(),
                countingOpenedTo: moment().add(2, 'h').toISOString()
            })).to.be.not.fulfilled;
        });
    });

    context('when handling AppendBallotCmd', function () {
        let voting: Voting;
        let ballot: DummyBallot;
        beforeEach(function () {
            voting = {
                votingId: 'voting#0',
                name: 'voting#0',
                mode: VotingMode.condorcet,
                owners,
                candidates,
                countingOpenedFrom: moment().add(1, 'h').toISOString(),
                countingOpenedTo: moment().add(2, 'h').toISOString()
            };
            ballot = {
                ballotId: 'ballot#0',
                content: [[candidates[0]], [candidates[0]]]
            }
        });
        it('should succeed when condorcet', async function () {
            sinon.stub(repository, 'findVoting').returns(Promise.resolve(voting));
            sinon.stub(repository, 'persistBallot').returns(Promise.resolve());
            await expect(handler.appendBallot({
                signer: owners[0],
                votingId: voting.name,
                ballot
            })).to.be.fulfilled;
        });
        it('should failed when signer not owner', async function () {
            sinon.stub(repository, 'findVoting').returns(Promise.resolve(voting));
            sinon.stub(repository, 'persistBallot').returns(Promise.resolve());
            await expect(handler.appendBallot({
                signer: 'unknown',
                votingId: voting.name,
                ballot
            })).to.be.not.fulfilled;
        });
        it('should failed when findVoting failed', async function () {
            sinon.stub(repository, 'findVoting').returns(Promise.reject(new Error()));
            sinon.stub(repository, 'persistBallot').returns(Promise.resolve());
            await expect(handler.appendBallot({
                signer: owners[0],
                votingId: voting.name,
                ballot
            })).to.be.not.fulfilled;
        });
        it('should failed when persistBallot failed', async function () {
            sinon.stub(repository, 'findVoting').returns(Promise.resolve(voting));
            sinon.stub(repository, 'persistBallot').returns(Promise.reject(new Error()));
            await expect(handler.appendBallot({
                signer: owners[0],
                votingId: voting.name,
                ballot
            })).to.be.not.fulfilled;
        });
    });

    context('when handling PublishResultCmd', function () {
        let voting: Voting;
        let result: OwnResult;
        beforeEach(function () {
            voting = {
                votingId: 'voting#0',
                name: 'voting#0',
                mode: VotingMode.condorcet,
                owners,
                candidates,
                countingOpenedFrom: moment().add(1, 'h').toISOString(),
                countingOpenedTo: moment().add(2, 'h').toISOString()
            };
            result = {
                owner: owners[0],
                scores: {candidate0: 20, candidate1: 10},
                ranking: [
                    {candidate: candidates[0], position: 0},
                    {candidate: candidates[1], position: 1}
                ]
            }
        });
        it('should succeed', async function () {
            sinon.stub(repository, 'findVoting').returns(Promise.resolve(voting));
            sinon.stub(repository, 'persistResult').returns(Promise.resolve());
            await expect(handler.publishResult({
                signer: owners[0],
                votingId: voting.name,
                scores: {candidate0: 20, candidate1: 10},
                ranking: [
                    {candidate: candidates[0], position: 0},
                    {candidate: candidates[1], position: 1}
                ]
            })).to.be.fulfilled;
        });
        it('should failed when signer not owner', async function () {
            sinon.stub(repository, 'findVoting').returns(Promise.resolve(voting));
            sinon.stub(repository, 'persistResult').returns(Promise.resolve());
            await expect(handler.publishResult({
                signer: 'unknown',
                votingId: voting.name,
                scores: {candidate0: 20, candidate1: 10},
                ranking: [
                    {candidate: candidates[0], position: 0},
                    {candidate: candidates[1], position: 1}
                ]
            })).to.be.not.fulfilled;
        });
        it('should failed when scores is not valid', async function () {
            sinon.stub(repository, 'findVoting').returns(Promise.resolve(voting));
            sinon.stub(repository, 'persistResult').returns(Promise.resolve());
            await expect(handler.publishResult({
                signer: owners[0],
                votingId: voting.name,
                scores: {
                    'candidate0': 0
                },
                ranking: result.ranking
            })).to.be.not.fulfilled;
        });
        it('should failed when one candidate is missing in the ranking', async function () {
            sinon.stub(repository, 'findVoting').returns(Promise.resolve(voting));
            sinon.stub(repository, 'persistResult').returns(Promise.resolve());
            await expect(handler.publishResult({
                signer: owners[0],
                votingId: voting.name,
                scores: result.scores,
                ranking: [{candidate: 'candidate1', position: 0}]
            })).to.be.not.fulfilled;
        });
        it('should failed when an unknown candidate is in the ranking', async function () {
            sinon.stub(repository, 'findVoting').returns(Promise.resolve(voting));
            sinon.stub(repository, 'persistResult').returns(Promise.resolve());
            await expect(handler.publishResult({
                signer: owners[0],
                votingId: voting.name,
                scores: result.scores,
                ranking: [
                    {candidate: 'candidate0', position: 0},
                    {candidate: 'candidate1', position: 0},
                    {candidate: 'unknown', position: 0}
                ]
            })).to.be.not.fulfilled;
        });
        it('should failed when findVoting failed', async function () {
            sinon.stub(repository, 'findVoting').returns(Promise.reject(new Error()));
            sinon.stub(repository, 'persistResult').returns(Promise.resolve());
            await expect(handler.publishResult({
                signer: owners[0],
                votingId: voting.name,
                scores: {candidate0: 20, candidate1: 10},
                ranking: [
                    {candidate: candidates[0], position: 0},
                    {candidate: candidates[1], position: 1}
                ]
            })).to.be.not.fulfilled;
        });
        it('should failed when persistResult failed', async function () {
            sinon.stub(repository, 'findVoting').returns(Promise.resolve(voting));
            sinon.stub(repository, 'persistResult').returns(Promise.reject(new Error()));
            await expect(handler.publishResult({
                signer: owners[0],
                votingId: voting.name,
                scores: {candidate0: 20, candidate1: 10},
                ranking: [
                    {candidate: candidates[0], position: 0},
                    {candidate: candidates[1], position: 1}
                ]
            })).to.be.not.fulfilled;
        });
    });
});
