import {expect} from 'chai';
import {Readable} from 'stream';
import {condorcet, CondorcetResult} from '../src/condorcet';

describe('condorcet', () => {

    it('should resolve https://en.wikipedia.org/wiki/Condorcet_method', function (done) {
        const candidates = ['m', 'n', 'c', 'k'];
        const ballots: any[] = [];
        for (let i = 0; i < 42; i++) {
            ballots.push({content: [['m'], ['n'], ['c'], ['k']]})
        }
        for (let i = 0; i < 26; i++) {
            ballots.push({content: [['n'], ['c'], ['k'], ['m']]})
        }
        for (let i = 0; i < 15; i++) {
            ballots.push({content: [['c'], ['k'], ['n'], ['m']]})
        }
        for (let i = 0; i < 17; i++) {
            ballots.push({content: [['k'], ['c'], ['n'], ['m']]})
        }

        const streamOfBallots = new Readable({objectMode: true});
        streamOfBallots.pipe(condorcet(candidates, {step: 5})).once('data', (evt: CondorcetResult) => {
            expect(evt.scores).to.have.nested.property('n', 3);
            expect(evt.scores).to.have.nested.property('c', 2);
            expect(evt.scores).to.have.nested.property('k', 1);
            expect(evt.scores).to.have.nested.property('m', 0);

            expect(evt.ranking).to.have.nested.property('[0].candidate', 'n');
            expect(evt.ranking).to.have.nested.property('[0].position', 0);
            expect(evt.ranking).to.have.nested.property('[0].won.length', 3);
            expect(evt.ranking).to.have.nested.property('[0].won[0].result.left', 42);
            expect(evt.ranking).to.have.nested.property('[0].lost.length', 0);

            expect(evt.ranking).to.have.nested.property('[1].candidate', 'c');
            expect(evt.ranking).to.have.nested.property('[1].position', 1);
            expect(evt.ranking).to.have.nested.property('[1].won.length', 2);
            expect(evt.ranking).to.have.nested.property('[1].lost.length', 1);

            expect(evt.ranking).to.have.nested.property('[2].candidate', 'k');
            expect(evt.ranking).to.have.nested.property('[2].position', 2);
            expect(evt.ranking).to.have.nested.property('[2].won.length', 1);
            expect(evt.ranking).to.have.nested.property('[2].lost.length', 2);

            expect(evt.ranking).to.have.nested.property('[3].candidate', 'm');
            expect(evt.ranking).to.have.nested.property('[3].position', 3);
            expect(evt.ranking).to.have.nested.property('[3].won.length', 0);
            expect(evt.ranking).to.have.nested.property('[3].lost.length', 3);

            done();
        });
        [...ballots, null].forEach(b => streamOfBallots.push(b));
    });

    it('should resolve multiple winners skipping not valid ballots', function (done) {
        const candidates = ['a', 'b', 'c'];
        const ballots = [
            {content: [['a'], ['b'], ['c']]},
            {content: [['b'], ['c'], ['a']]},
            {content: [['c'], ['a'], ['b']]}
        ];

        const streamOfBallots = new Readable({objectMode: true});
        streamOfBallots.pipe(condorcet(candidates)).once('data', (evt: CondorcetResult) => {
            expect(evt.scores).to.have.nested.property('a', 1);
            expect(evt.scores).to.have.nested.property('b', 1);
            expect(evt.scores).to.have.nested.property('c', 1);

            expect(evt.ranking).to.have.nested.property('[0].candidate', 'a');
            expect(evt.ranking).to.have.nested.property('[0].position', 0);
            expect(evt.ranking).to.have.nested.property('[0].won.length', 1);
            expect(evt.ranking).to.have.nested.property('[0].lost.length', 1);

            expect(evt.ranking).to.have.nested.property('[1].candidate', 'b');
            expect(evt.ranking).to.have.nested.property('[1].position', 0);
            expect(evt.ranking).to.have.nested.property('[1].won.length', 1);
            expect(evt.ranking).to.have.nested.property('[1].lost.length', 1);

            expect(evt.ranking).to.have.nested.property('[2].candidate', 'c');
            expect(evt.ranking).to.have.nested.property('[2].position', 0);
            expect(evt.ranking).to.have.nested.property('[2].won.length', 1);
            expect(evt.ranking).to.have.nested.property('[2].lost.length', 1);

            done();
        });

        [...ballots, null].forEach(b => streamOfBallots.push(b));
    });

    it('should allow to rank candidates as equals', function (done) {
        const candidates = ['a', 'b'];
        const ballots = [
            {content: [['a', 'b']]},
            {content: [['a'], ['b']]}
        ];

        const streamOfBallots = new Readable({objectMode: true});
        streamOfBallots.pipe(condorcet(candidates)).once('data', (evt: CondorcetResult) => {
            expect(evt.scores).to.have.nested.property('a', 1);
            expect(evt.scores).to.have.nested.property('b', 0);

            expect(evt.ranking).to.have.nested.property('[0].candidate', 'a');
            expect(evt.ranking).to.have.nested.property('[0].position', 0);
            expect(evt.ranking).to.have.nested.property('[0].won.length', 1);
            expect(evt.ranking).to.have.nested.property('[0].lost.length', 0);

            expect(evt.ranking).to.have.nested.property('[1].candidate', 'b');
            expect(evt.ranking).to.have.nested.property('[1].position', 1);
            expect(evt.ranking).to.have.nested.property('[1].won.length', 0);
            expect(evt.ranking).to.have.nested.property('[1].lost.length', 1);

            done();
        });

        [...ballots, null].forEach(b => streamOfBallots.push(b));
    });

    it('should allow candidates omitted in ballots', function (done) {
        const candidates = ['a', 'b', 'c'];
        const ballots = [
            {content: [['a'], ['b'], ['c']]},
            {content: [['a'], ['c']]}
        ];

        const streamOfBallots = new Readable({objectMode: true});
        streamOfBallots.pipe(condorcet(candidates)).once('data', (evt: CondorcetResult) => {
            expect(evt.scores).to.have.nested.property('a', 2);
            expect(evt.scores).to.have.nested.property('b', 0);
            expect(evt.scores).to.have.nested.property('c', 0);

            expect(evt.ranking).to.have.nested.property('[0].candidate', 'a');
            expect(evt.ranking).to.have.nested.property('[0].position', 0);
            expect(evt.ranking).to.have.nested.property('[0].won.length', 2);
            expect(evt.ranking).to.have.nested.property('[0].lost.length', 0);

            expect(evt.ranking).to.have.nested.property('[1].candidate', 'b');
            expect(evt.ranking).to.have.nested.property('[1].position', 1);
            expect(evt.ranking).to.have.nested.property('[1].won.length', 0);
            expect(evt.ranking).to.have.nested.property('[1].lost.length', 1);

            expect(evt.ranking).to.have.nested.property('[2].candidate', 'c');
            expect(evt.ranking).to.have.nested.property('[2].position', 1);
            expect(evt.ranking).to.have.nested.property('[2].won.length', 0);
            expect(evt.ranking).to.have.nested.property('[2].lost.length', 1);

            done();
        });

        [...ballots, null].forEach(b => streamOfBallots.push(b));
    });

});
