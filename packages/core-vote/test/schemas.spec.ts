import {expect, use} from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as moment from 'moment';
import {
    AppendBallotCmdSchema,
    IdentifiedBallotSchema,
    OwnResultSchema,
    PublishResultSchema,
    RegisterVotingCmdSchema
} from '../src/schemas';

use(chaiAsPromised);

describe('schemas', () => {

    context('RegisterVotingCmdSchema', () => {
        it('should succeed', async function () {
            await expect(RegisterVotingCmdSchema.validate({
                signer: 'signer1',
                votingId: 'votingId',
                name: 'votingName',
                mode: 'condorcet',
                owners: ['signer1', 'signer2'],
                candidates: ['candidate1', 'candidate2'],
                countingOpenedFrom: moment().add(1, 'h').toISOString(),
                countingOpenedTo: moment().add(2, 'h').toISOString()
            })).to.be.fulfilled;
        });
        it('should failed when no signer', async function () {
            await expect(RegisterVotingCmdSchema.validate({
                signer: '',
                votingId: 'votingId',
                name: 'votingName',
                mode: 'condorcet',
                owners: ['signer1', 'signer2'],
                candidates: ['candidate1', 'candidate2'],
                countingOpenedFrom: moment().add(1, 'h').toISOString(),
                countingOpenedTo: moment().add(2, 'h').toISOString()
            })).to.be.not.fulfilled;
        });
        it('should failed when no voting id', async function () {
            await expect(RegisterVotingCmdSchema.validate({
                signer: 'signer1',
                votingId: '',
                name: 'name',
                mode: 'condorcet',
                owners: ['signer1', 'signer2'],
                candidates: ['candidate1', 'candidate2'],
                countingOpenedFrom: moment().add(1, 'h').toISOString(),
                countingOpenedTo: moment().add(2, 'h').toISOString()
            })).to.be.not.fulfilled;
        });
        it('should failed when no voting name', async function () {
            await expect(RegisterVotingCmdSchema.validate({
                signer: 'signer1',
                votingId: 'votingId',
                name: '',
                mode: 'condorcet',
                owners: ['signer1', 'signer2'],
                candidates: ['candidate1', 'candidate2'],
                countingOpenedFrom: moment().add(1, 'h').toISOString(),
                countingOpenedTo: moment().add(2, 'h').toISOString()
            })).to.be.not.fulfilled;
        });
        it('should failed when no mode', async function () {
            await expect(RegisterVotingCmdSchema.validate({
                signer: 'signer1',
                votingId: 'votingId',
                name: 'votingName',
                mode: '',
                owners: ['signer1', 'signer2'],
                candidates: ['candidate1', 'candidate2'],
                countingOpenedFrom: moment().add(1, 'h').toISOString(),
                countingOpenedTo: moment().add(2, 'h').toISOString()
            })).to.be.not.fulfilled;
        });
        it('should failed when no owners', async function () {
            await expect(RegisterVotingCmdSchema.validate({
                signer: 'signer1',
                votingId: 'votingId',
                name: 'votingName',
                mode: 'condorcet',
                owners: [],
                candidates: ['candidate1', 'candidate2'],
                countingOpenedFrom: moment().add(1, 'h').toISOString(),
                countingOpenedTo: moment().add(2, 'h').toISOString()
            })).to.be.not.fulfilled;
        });
        it('should failed when no signer in owners', async function () {
            await expect(RegisterVotingCmdSchema.validate({
                signer: 'signer0',
                votingId: 'votingId',
                name: 'votingName',
                mode: 'condorcet',
                owners: ['signer1', 'signer2'],
                candidates: ['candidate1', 'candidate2'],
                countingOpenedFrom: moment().add(1, 'h').toISOString(),
                countingOpenedTo: moment().add(2, 'h').toISOString()
            })).to.be.not.fulfilled;
        });
        it('should failed when no candidates', async function () {
            await expect(RegisterVotingCmdSchema.validate({
                signer: 'signer1',
                votingId: 'votingId',
                name: 'votingName',
                mode: 'condorcet',
                owners: ['signer1', 'signer2'],
                candidates: [],
                countingOpenedFrom: moment().add(1, 'h').toISOString(),
                countingOpenedTo: moment().add(2, 'h').toISOString()
            })).to.be.not.fulfilled;
        });
        it('should failed when no countingOpenedFrom', async function () {
            await expect(RegisterVotingCmdSchema.validate({
                signer: 'signer1',
                votingId: 'votingId',
                name: 'votingName',
                mode: 'condorcet',
                owners: ['signer1', 'signer2'],
                candidates: ['candidate1', 'candidate2'],
                countingOpenedFrom: '',
                countingOpenedTo: moment().add(2, 'h').toISOString()
            })).to.be.not.fulfilled;
        });
        it('should failed when no countingOpenedTo', async function () {
            await expect(RegisterVotingCmdSchema.validate({
                signer: 'signer1',
                name: 'votingName',
                mode: 'condorcet',
                owners: ['signer1', 'signer2'],
                candidates: ['candidate1', 'candidate2'],
                countingOpenedFrom: moment().add(1, 'h').toISOString(),
                countingOpenedTo: ''
            })).to.be.not.fulfilled;
        });
        it('should failed when countingOpenedFrom in the past', async function () {
            await expect(RegisterVotingCmdSchema.validate({
                signer: 'signer1',
                votingId: 'votingId',
                name: 'votingName',
                mode: 'condorcet',
                owners: ['signer1', 'signer2'],
                candidates: ['candidate1', 'candidate2'],
                countingOpenedFrom: moment().add(-1, 'h').toISOString(),
                countingOpenedTo: moment().add(2, 'h').toISOString()
            })).to.be.not.fulfilled;
        });
        it('should failed when countingOpenedTo is before countingOpenedFrom', async function () {
            await expect(RegisterVotingCmdSchema.validate({
                signer: 'signer1',
                votingId: 'votingId',
                name: 'votingName',
                mode: 'condorcet',
                owners: ['signer1', 'signer2'],
                candidates: ['candidate1', 'candidate2'],
                countingOpenedFrom: moment().add(1, 'h').toISOString(),
                countingOpenedTo: moment().add(-2, 'h').toISOString()
            })).to.be.not.fulfilled;
        });
    });

    context('AppendBallotCmdSchema', () => {
        it('should succeed with condorcet ballot', async function () {
            await expect(AppendBallotCmdSchema.validate({
                signer: 'signer1',
                votingId: 'votingId',
                ballot: {
                    ballotId: 'identifier',
                    content: [['candidate1'], ['candidate2']]
                }
            })).to.be.fulfilled;
        });
        it('should failed when no signer', async function () {
            await expect(AppendBallotCmdSchema.validate({
                signer: '',
                votingId: 'votingId',
                ballot: {
                    ballotId: 'identifier',
                    content: [['candidate1'], ['candidate2']]
                }
            })).to.be.not.fulfilled;
        });
        it('should failed when no voting id', async function () {
            await expect(AppendBallotCmdSchema.validate({
                signer: 'signer1',
                votingId: '',
                ballot: {
                    ballotId: 'identifier',
                    content: [['candidate1'], ['candidate2']]
                }
            })).to.be.not.fulfilled;
        });
        it('should failed when no ballot', async function () {
            await expect(AppendBallotCmdSchema.validate({
                signer: 'signer1',
                votingId: 'votingId',
            })).to.be.not.fulfilled;
        });
    });

    context('IdentifiedBallotSchema', () => {
        it('should succeed', async function () {
            await expect(IdentifiedBallotSchema.validate({
                ballotId: 'identifier',
                content: [[], []]
            }, {context: {mode: 'condorcet', candidates: ['candidate1', 'candidate2']}})).to.be.fulfilled;
        });
        it('should failed when no ballotId', async function () {
            await expect(IdentifiedBallotSchema.validate({
                ballotId: '',
                content: [['candidate1'], ['candidate2']]
            }, {context: {mode: 'condorcet'}})).to.be.not.fulfilled;
        });
        context('condorcet', () => {
            it('should succeed when no candidates', async function () {
                await expect(IdentifiedBallotSchema.validate({
                    ballotId: 'identifier',
                    content: [[]]
                }, {context: {mode: 'condorcet', candidates: ['candidate1', 'candidate2']}})).to.be.fulfilled;
            });
            it('should succeed when not all candidates', async function () {
                await expect(IdentifiedBallotSchema.validate({
                    ballotId: 'identifier',
                    content: [[], ['candidate2'], []]
                }, {context: {mode: 'condorcet', candidates: ['candidate1', 'candidate2']}})).to.be.fulfilled;
            });
            it('should failed when candidate unknown', async function () {
                await expect(IdentifiedBallotSchema.validate({
                    ballotId: 'identifier',
                    content: [['unknown'], ['candidate2']]
                }, {context: {mode: 'condorcet', candidates: ['candidate1', 'candidate2']}})).to.be.not.fulfilled;
            });
        });
    });

    context('PublishResultSchema', () => {
        it('should succeed', async function () {
            await expect(PublishResultSchema.validate({
                signer: 'signer1',
                votingId: 'votingId',
                scores: {candidate0: 0},
                ranking: [{candidate: 'candidate0', position: 0}]
            })).to.be.fulfilled;
        });
        it('should failed when no signer', async function () {
            await expect(PublishResultSchema.validate({
                signer: '',
                votingId: 'votingId',
                scores: {candidate0: 0},
                ranking: [{candidate: 'candidate0', position: 0}]
            })).to.be.not.fulfilled;
        });
        it('should failed when no voting id', async function () {
            await expect(PublishResultSchema.validate({
                signer: 'signer1',
                votingId: '',
                scores: {candidate0: 0},
                ranking: [{candidate: 'candidate0', position: 0}]
            })).to.be.not.fulfilled;
        });
        it('should failed when no scores', async function () {
            await expect(PublishResultSchema.validate({
                signer: 'signer1',
                votingId: 'votingId',
                ranking: [{candidate: 'candidate0', position: 0}]
            })).to.be.not.fulfilled;
        });
        it('should failed when no ranking', async function () {
            await expect(PublishResultSchema.validate({
                signer: 'signer1',
                votingId: 'votingId',
                scores: {candidate0: 0}
            })).to.be.not.fulfilled;
        });
    });

    context('OwnResultSchema', () => {
        it('should succeed', async function () {
            await expect(OwnResultSchema.validate({
                owner: 'owner1',
                scores: {candidate0: 0},
                ranking: [{candidate: 'candidate0', position: 0}]
            }, {context: {owners: ['owner1']}})).to.be.fulfilled;
        });
        it('should failed when no owner', async function () {
            await expect(OwnResultSchema.validate({
                owner: '',
                scores: {candidate0: 0},
                ranking: [{candidate: 'candidate0', position: 0}]
            }, {context: {owners: ['owner1']}})).to.be.not.fulfilled;
        });
        it('should failed when owner not valid', async function () {
            await expect(OwnResultSchema.validate({
                owner: 'owner0',
                scores: {candidate0: 0},
                ranking: [{candidate: 'candidate0', position: 0}]
            }, {context: {owners: ['owner1']}})).to.be.not.fulfilled;
        });
        it('should failed when no scores', async function () {
            await expect(OwnResultSchema.validate({
                owner: 'owner1',
                ranking: [{candidate: 'candidate0', position: 0}]
            }, {context: {owners: ['owner1']}})).to.be.not.fulfilled;
        });
        it('should failed when no ranking', async function () {
            await expect(OwnResultSchema.validate({
                owner: 'owner1',
                scores: {candidate0: 0}
            }, {context: {owners: ['owner1']}})).to.be.not.fulfilled;
        });
        it('should failed when ranking not valid', async function () {
            await expect(OwnResultSchema.validate({
                owner: 'owner1',
                scores: {candidate0: 0},
                ranking: [{candidate: 'candidate0'}]
            }, {context: {owners: ['owner1']}})).to.be.not.fulfilled;
        });
    });
});
