import {DefaultHandler, IdentifiedBallot, Repository} from '../src/commands';
import {RankingBallot} from '../src/ballot';
import {OwnResult} from '../src/commands';
import {Voting} from '../src/voting';

export interface DummyBallot extends RankingBallot, IdentifiedBallot<Array<Array<string>>> {
}

export class DummyHandler extends DefaultHandler<DummyBallot> {
    constructor(repository: Repository<DummyBallot>) {
        super(repository);
    }
}

export class DummyRepository implements Repository<DummyBallot> {
    async findBallot(votingId: string, identifier: string): Promise<DummyBallot> {
        throw new Error('not implemented');
    }

    async findResult(votingId: string, owner: string): Promise<Readonly<OwnResult>> {
        throw new Error('not implemented');
    }

    async findVoting(votingId: string): Promise<Voting> {
        throw new Error('not implemented');
    }

    async persistBallot(voting: Voting, ballot: DummyBallot): Promise<void> {
        throw new Error('not implemented');
    }

    async persistResult(voting: Voting, result: OwnResult): Promise<void> {
        throw new Error('not implemented');
    }

    async persistVoting(voting: Voting): Promise<void> {
        throw new Error('not implemented');
    }

}
