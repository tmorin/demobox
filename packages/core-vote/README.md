# `@demobox/core-vote`

> Implementation of voting systems.

## Install

```bash
npm install @demobox/core-vote
```

## Usage

### Condorcet

Implementation of the [Condorcet](https://en.wikipedia.org/wiki/Condorcet_method) system.

```javascript
import {Readable} from 'stream';
import {computeCondorcet} from '@demobox/core-vote';

// define the available candidates
const candidates = ['a', 'b', 'c'];

// define the ballots
const ballots = [
    {ballotId: 'A', content: [['a'], ['b'], ['c']]},
    {ballotId: 'B', content: [['b'], ['c'], ['a']]},
    {ballotId: 'C', content: [['c'], ['a'], ['b']]}
];

// create a readable stream
const streamOfBallots = new Readable({
    objectMode: true
});

// compute and get the results
computeCondorcet(candidates, streamOfBallots).once('done', result => {
    console.log(result.scores);
    // { a: 1, b: 1, c: 1 }

    console.log(result.rankings);
    // [ { candidate: 'a', position: 0, won: [ [CondorcetPair] ], lost: [ [CondorcetPair] ] },
    //   { candidate: 'b', position: 0, won: [ [CondorcetPair] ], lost: [ [CondorcetPair] ] },
    //   { candidate: 'c', position: 0, won: [ [CondorcetPair] ], lost: [ [CondorcetPair] ] } ]
});

[...ballots, null].forEach(b => streamOfBallots.push(b));
```
