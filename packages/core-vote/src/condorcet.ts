import * as joi from 'joi';
import {Transform, TransformCallback} from 'stream';
import {Rank, Scores} from './result';
import {RankingBallot} from './ballot';
import {RankingBallotSchema} from './schemas';

function getRank(ranking: Array<Array<string>>, candidate: string) {
    const rank = ranking.findIndex(items => items.indexOf(candidate) > -1);
    return rank < 0 ? ranking.length : rank;
}

export interface CondorcetPairResult {
    left: number
    right: number
}

export class CondorcetPair {
    constructor(
        public readonly left: string,
        public readonly right: string,
        private _counterLeft: number = 0,
        private _counterRight: number = 0
    ) {
    }

    get winner(): string | null {
        if (this._counterLeft > this._counterRight) {
            return this.left;
        }
        if (this._counterLeft < this._counterRight) {
            return this.right;
        }
        return null;
    }

    get result(): CondorcetPairResult {
        return {
            left: this._counterLeft,
            right: this._counterRight
        }
    }

    vote(side: string): CondorcetPair {
        if (side == this.left) {
            this._counterLeft = this._counterLeft + 1;
        } else {
            this._counterRight = this._counterRight + 1;
        }
        return this;
    }

    matchSides(left: string, right: string): boolean {
        return [this.left, this.right].indexOf(left) > -1 && [this.left, this.right].indexOf(right) > -1
    }

    matchSide(side: string) {
        return [this.left, this.right].indexOf(side) > -1;
    }
}

export interface CondorcetRank extends Rank {
    won: ReadonlyArray<CondorcetPair>
    lost: ReadonlyArray<CondorcetPair>
}

export interface CondorcetResult {
    scores: Readonly<Scores>
    ranking: ReadonlyArray<Readonly<CondorcetRank>>
}

export interface CondorcetOptions {
    step: number
}

const DEFAULT_OPTIONS: CondorcetOptions = {
    step: 1000
};

export class CondorcetStream extends Transform {

    private readonly _options: CondorcetOptions;
    private readonly _scores: Scores;
    private readonly _ranking: Array<CondorcetRank>;
    private readonly _pairs: Array<CondorcetPair>;
    private _counter = 0;

    constructor(
        private readonly _candidates: Array<string>,
        options: CondorcetOptions = DEFAULT_OPTIONS
    ) {
        super({
            readableObjectMode: true,
            writableObjectMode: true
        });
        // initialize the options
        this._options = {...DEFAULT_OPTIONS, ...options};

        // initialize the score to 0 for each candidate
        this._scores = _candidates.reduce((scores, candidate) => {
            scores[candidate] = 0;
            return scores;
        }, {} as Scores);

        // initialize the ranking of each candidate
        this._ranking = _candidates.map(
            candidate => ({candidate: candidate, position: 0, won: [], lost: [], winner: false})
        );

        // initialize the pairs (i.e. the match between each candidate)
        this._pairs = [];
    }

    _transform(chunk: RankingBallot, encoding: string, callback: TransformCallback): void {
        // validation of the ballot
        joi.validate(chunk, RankingBallotSchema, {context: {candidates: this._candidates}})
            .then(ballot => {
                // for each ballot compute the result for all pairs
                this._candidates.forEach((left, leftI) => {
                    this._candidates.forEach((right, rightI) => {
                        const rankLeft = getRank(ballot.content, left);
                        const rankRight = getRank(ballot.content, right);
                        if (leftI !== rightI && rankLeft < rankRight) {
                            let pair = this._pairs.filter(p => p.matchSides(left, right))[0];
                            if (!pair) {
                                pair = new CondorcetPair(left, right);
                                this._pairs.push(pair);
                            }
                            pair.vote(left);
                        }
                    });
                });

                // increment the counter of processed ballots
                this._counter += 1;

                // emit the event progress when the number of processed ballots reach the step
                if (this._counter % this._options.step) {
                    this.emit('computing', this._counter);
                }

                callback();
            })
            .catch(callback);
    }

    _flush(callback: TransformCallback): void {
        // identify the winner of each pair
        this._pairs.map(pair => pair.winner).filter(winner => !!winner).reduce((scores, winner) => {
            scores[winner as string] += 1;
            return scores;
        }, this._scores as Scores);

        // compute the rank of each score
        const orderedScores = Object.keys(this._scores)
            .map(k => this._scores[k])
            .reduce((all, v) => {
                if (all.indexOf(v) < 0) {
                    all.push(v);
                }
                return all;
            }, [] as Array<number>)
            .sort((a, b) => b - a);

        // compute the ranking of the candidates
        Object.assign(this._ranking, this._candidates.map(candidate => {
            const won: Array<CondorcetPair> = [];
            const lost: Array<CondorcetPair> = [];
            this._pairs.filter(pair => pair.matchSide(candidate)).forEach(pair => {
                if (pair.winner === candidate) {
                    won.push(pair);
                } else if (pair.winner) {
                    lost.push(pair);
                }
            });
            const position = orderedScores.indexOf(won.length);
            return {candidate, position, won, lost};
        }).sort((a, b) => {
            const diff = b.won.length - a.won.length;
            return diff !== 0 ? diff : a.candidate.localeCompare(b.candidate);
        }));

        callback(undefined, Object.freeze({
            scores: Object.freeze(this._scores),
            ranking: Object.freeze(this._ranking)
        }));
    }
}

export function condorcet(
    candidates: Array<string>,
    options?: CondorcetOptions
) {
    return new CondorcetStream(candidates, options);
}
