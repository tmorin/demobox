import * as joi from 'joi';
import {VotingMode} from './voting';

export const RegisterVotingCmdSchema = joi.object()
    .keys({
        signer: joi.string().trim().required(),
        votingId: joi.string().trim().required(),
        name: joi.string().trim().required(),
        mode: joi.string().trim().lowercase().required().valid([VotingMode.condorcet]),
        owners: joi.array().required().items(joi.string().valid(joi.ref('signer')).required(), joi.string().trim()),
        candidates: joi.array().required().min(1),
        countingOpenedFrom: joi.date().iso().required().greater('now'),
        countingOpenedTo: joi.date().iso().required().greater(joi.ref('countingOpenedFrom'))
    });

export const VotingSchema = joi.object()
    .keys({
        votingId: joi.string().trim().required(),
        name: joi.string().trim().required(),
        mode: joi.string().trim().lowercase().required(),
        owners: joi.array().required().items(joi.string().trim().required(), joi.string().trim()),
        candidates: joi.array().required().min(1),
        countingOpenedFrom: joi.date().iso().required(),
        countingOpenedTo: joi.date().iso().required()
    });

export const AppendBallotCmdSchema = joi.object()
    .keys({
        signer: joi.string().trim().required(),
        votingId: joi.string().trim().required(),
        ballot: joi.any().required()
    });

export const RankingBallotSchema = joi.object()
    .keys({
        content: joi.array().required().items(joi.array().required().items(
            joi.string().trim().valid(joi.ref('$candidates'))
        ))
    });

export const IdentifiedBallotSchema = joi.object()
    .keys({
        ballotId: joi.string().trim().required()
    })
    .when(
        joi.ref('$mode'),
        {is: joi.string().valid(VotingMode.condorcet), then: RankingBallotSchema}
    );

export const RankSchema = joi.object().keys({
    candidate: joi.string().required(),
    position: joi.number().required().min(0)
});

export const OwnResultSchema = joi.object()
    .keys({
        owner: joi.string().required().trim().valid(joi.ref('$owners')),
        scores: joi.object().required(),
        ranking: joi.array().required().items(RankSchema.required())
    });

export const PublishResultSchema = joi.object()
    .keys({
        signer: joi.string().trim().required(),
        votingId: joi.string().trim().required(),
        scores: joi.object().required(),
        ranking: joi.array().required().items(RankSchema.required())
    });
