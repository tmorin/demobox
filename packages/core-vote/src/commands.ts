import * as joi from 'joi';
import {Voting, VotingMode} from './voting';
import {Rank, Result, Scores} from './result';
import {Ballot} from './ballot';
import {
    AppendBallotCmdSchema,
    IdentifiedBallotSchema,
    OwnResultSchema,
    PublishResultSchema,
    RankSchema,
    RegisterVotingCmdSchema,
    VotingSchema
} from './schemas';

export interface IdentifiedBallot<C> extends Ballot<C> {

    ballotId: string

}

export interface OwnResult extends Result {

    owner: string

}

export interface RegisterVotingCmd {

    signer: string

    votingId: string

    name: string

    mode: VotingMode

    owners: Array<string>

    candidates: Array<string>

    countingOpenedFrom: string

    countingOpenedTo: string

}

export interface AppendBallotCmd<B extends IdentifiedBallot<any>> {

    signer: string

    votingId: string

    ballot: B

}

export interface PublishResultCmd {

    signer: string

    votingId: string

    scores: Readonly<Scores>

    ranking: ReadonlyArray<Rank>

}

export interface Handler<B extends IdentifiedBallot<any>> {

    registerVoting(cmd: RegisterVotingCmd): Promise<void>

    appendBallot(cmd: AppendBallotCmd<B>): Promise<void>

    publishResult(cmd: PublishResultCmd): Promise<void>

}

export interface Repository<B extends IdentifiedBallot<any>> {

    findVoting(votingId: string): Promise<Readonly<Voting>>;

    persistVoting(voting: Voting): Promise<void>;

    findBallot(votingId: string, identifier: string): Promise<Readonly<B>>;

    persistBallot(voting: Voting, ballot: B): Promise<void>;

    findResult(votingId: string, owner: string): Promise<Readonly<OwnResult>>;

    persistResult(voting: Voting, result: OwnResult): Promise<void>;

}

export class DefaultHandler<B extends IdentifiedBallot<any>> implements Handler<B> {

    public constructor(
        private readonly repository: Repository<IdentifiedBallot<any>>
    ) {
    }

    async registerVoting(cmd: RegisterVotingCmd): Promise<void> {
        // validation of the command
        await RegisterVotingCmdSchema.validate(cmd);

        // validation of the voting entity
        const voting = await VotingSchema.validate({
            votingId: cmd.votingId,
            name: cmd.name,
            mode: cmd.mode,
            owners: Object.freeze(cmd.owners),
            candidates: Object.freeze(cmd.candidates),
            countingOpenedFrom: cmd.countingOpenedFrom,
            countingOpenedTo: cmd.countingOpenedTo
        } as Voting);

        // persist the voting entity
        await this.repository.persistVoting(voting);
    }

    async appendBallot(cmd: AppendBallotCmd<B>): Promise<void> {
        // validation of the command
        await AppendBallotCmdSchema.validate(cmd);

        // get voting
        const voting = await this.repository.findVoting(cmd.votingId);

        // check signer can append ballot
        await joi.validate(
            voting.owners,
            joi.array().required().items(joi.string().required().valid(joi.ref('$signer')), joi.string()),
            {context: {signer: cmd.signer}}
        );

        // validation of the ballot
        const ballot = await IdentifiedBallotSchema.validate(cmd.ballot, {
            context: {mode: voting.mode, candidates: voting.candidates}
        });

        // persist the ballot
        await this.repository.persistBallot(voting, ballot);
    }

    async publishResult(cmd: PublishResultCmd): Promise<void> {
        // validation of the command
        await PublishResultSchema.validate(cmd);

        // get voting
        const voting = await this.repository.findVoting(cmd.votingId);

        // check signer can publish result
        await joi.validate(
            voting.owners,
            joi.array().required().items(joi.string().required().valid(joi.ref('$signer')), joi.string()),
            {context: {signer: cmd.signer}}
        );

        // validation of the result
        const result = await OwnResultSchema.validate({
            owner: cmd.signer,
            scores: Object.freeze(cmd.scores),
            ranking: Object.freeze(cmd.ranking)
        }, {
            context: {owners: voting.owners}
        });

        // validation of the scores
        await joi.validate(
            result.scores,
            joi.object().keys(voting.candidates.reduce(
                (r, k) => Object.assign(r, {[k]: joi.number().required().min(0)}),
                {} as any
            ))
        );

        // validation of the ranking
        await joi.validate(
            result.ranking,
            joi.array().required().length(voting.candidates.length).items(voting.candidates.map(candidate =>
                RankSchema.required().keys({candidate: joi.string().trim().valid(candidate).required()}))
            )
        );

        // persist the result
        await this.repository.persistResult(voting, result);
    }

}
