export interface Scores {

    [candidate: string]: number

}

export interface Rank {

    candidate: string

    position: number

}

export interface Result {

    scores: Readonly<Scores>

    ranking: ReadonlyArray<Readonly<Rank>>

}

