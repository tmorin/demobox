export interface Ballot<C> {

    content: C

}

export interface RankingBallot extends Ballot<Array<Array<string>>> {
}
