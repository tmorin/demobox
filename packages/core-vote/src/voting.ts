export enum VotingMode {

    condorcet = 'condorcet'

}

export interface Voting {

    votingId: string

    name: string

    mode: VotingMode

    owners: ReadonlyArray<string>

    candidates: ReadonlyArray<string>

    countingOpenedFrom: string

    countingOpenedTo: string

}
